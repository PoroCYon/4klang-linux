# Linux templates for PC intro synths

Small template code to get 4klang, Clinkster, Oidos and V2 running on Linux :)

Quickly hacked together by PoroCYon.

Hugs to Alkama and noby for providing 4klang test tracks! And greets to
Punqtured for publishing the `.xrns` files too in [Tiny Sound
Planet](https://demozoo.org/productions/172416/).

## How to use

### Requirements

* A C compiler, make, sed, awk
* nasm or yasm
* Python **2**, for the converters used in Clinkster and Oidos.

### Usage

#### 4klang

1. Put the exported 4klang files in the `mus` folder (`4klang.asm`,
   `4klang.inc` and `4klang.h`).
2. Run `make`
3. Run `bin/runner`

#### Clinkster

1. Put the `.xrns` files in the `mus` folder.
2. Run `make`
3. Run `bin/play-<songname> | aplay`

#### Oidos

1. Put the `.xrns` files in the `mus` folder.
2. Run `make`
3. Run `bin/play-<songname> | aplay`

#### V2

1. Put the exported `.v2m` files in the `mus` folder.
2. Run `make`
3. Run `bin/lplayer-<songname> | aplay -c2 -r44100 -fFLOAT_LE`

## License

[WTFPL](/LICENSE).

The code under the `clinkster/Clinkster` directory and all code in the git
submodules, are all published under their respective licenses.

