
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#ifdef USE_CXX_VERSION
//#define AUTOCONVERT
#endif

#include "v2mplayer.h"
#include "synth.h"
#ifdef AUTOCONVERT
#include "v2mconv.h"
#endif
#ifdef USE_CXX_VERSION
#include "sounddef.h"
#endif

// automatically convert the module to the newest format version

extern "C" /*const*/ uint8_t theTune[];
extern "C" const size_t  theTune_size;

#define BUFFERLEN (8192) /* arbitrary */

static V2MPlayer player;
static float output_buffer[BUFFERLEN*8];

#ifdef AUTOCONVERT
static uint8_t* check_and_convert(/*const*/ uint8_t* tune, size_t len) {
	if (tune[2] != 0 || tune[3] != 0) {
		fprintf(stderr, "Invalid input file\n");
		return NULL;
	}
	int v = CheckV2MVersion(tune, len);
	if (!v) return tune; // version is the latest one
	if (v < 0) {
		fprintf(stderr, "Invalid input file (version not recognised)\n");
		return NULL;
	}

	uint8_t* theTune2;
	int len2;

	ConvertV2M(tune, len, &theTune2, &len2);
	return theTune2;
}
#endif

int main() {
#ifdef USE_CXX_VERSION
	sdInit();
#endif
#ifdef AUTOCONVERT
	uint8_t* theTune2 = check_and_convert(theTune, theTune_size);
	if (!theTune2) return 1;
#else
	const uint8_t* theTune2 = theTune;
#endif

	player.Init();
	player.Open(theTune2);

	player.Play();

	while (player.IsPlaying()) {
		/*
		 * Apparently, if I make the sample amount passed to Render larger,
		 * the synth will segfault somewhere in synth.asm, when writing the
		 * output samples back to the buffer. I'm not sure if/how this is
		 * supposed to happen, but keeping the sample count sufficiently low
		 * works around it. ¯\_(ツ)_/¯
		 * If you have a better fix, please tell me.
		 */
		const size_t samples = 64;
		player.Render(output_buffer, samples, false);
		write(STDOUT_FILENO, output_buffer, samples*sizeof(float)*2/*stereo*/);
	}

	player.Close();

	return 0;
}

