
#include <unistd.h>

#include "clinkster.h"

int main() {
	Clinkster_GenerateMusic();

	write(STDOUT_FILENO, Clinkster_WavFileHeader, sizeof(Clinkster_WavFileHeader));
	write(STDOUT_FILENO, Clinkster_MusicBuffer, Clinkster_WavFileHeader[10]);
}

