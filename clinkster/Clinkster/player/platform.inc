
%ifdef LINUX
%define PUBLIC_FN(n,i) n
%define PUBLIC_DATA(n) n
%elifdef WIN32
%define PUBLIC_FN(n,i) _ %+ n @ %+ i
%define PUBLIC_DATA(n) _ %+ n
%else
; mac osx
%define PUBLIC_FN(n,i) _ %+ n
%define PUBLIC_DATA(n) _ %+ n
%endif

%ifdef LINUX
%define SECT_BSS(n)   section    .bss.clinkster. %+ n   nobits alloc noexec   write
%define SECT_DATA(n)  section   .data.clinkster. %+ n progbits alloc noexec   write
%define SECT_RDATA(n) section .rodata.clinkster. %+ n progbits alloc noexec nowrite
%define SECT_TEXT(n)  section   .text.clinkster. %+ n progbits alloc   exec nowrite
%else
%define SECT_BSS(n)   section .clinkster. %+ n bss
%define SECT_DATA(n)  section .clinkster. %+ n data
%define SECT_RDATA(n) section .clinkster. %+ n rdata
%define SECT_TEXT(n)  section .clinkster. %+ n code
%endif

